defmodule BarrexTest do

  use ExUnit.Case

  setup do
    :barrel_httpc.delete_database(db_url())
    :barrel_httpc.create_database(db_url())
    {:ok, db} = Barrex.Database.open(db_url())
    [db: db]
  end

  describe "create_database" do

    test "create a new database" do
      tempdb = barrel_url() <> "/dbs/tempdb"
      :barrel_httpc.delete_database(tempdb)
      assert :ok = Barrex.create_database(tempdb)
    end

  end

  describe "delete_database" do

    test "delete database" do
      assert :ok = Barrex.delete_database(db_url())
    end

  end

  describe "get" do

    test "returns :not_found if document id is not found in the database", context do
      db = context[:db]
      assert {:error, :not_found} = Barrex.Database.get("doesnotexist", db)
    end

    test "returns {:ok, doc, meta} when id is in the database", context do
      db = context[:db]
      doc = %{"name" => "john", "v" => 1}
      {:ok, docid, _} =  Barrex.Database.post(doc, db)
      doc_with_id = Map.put(doc, "id", docid)
      {:ok, doc_retrieved, _meta} = Barrex.Database.get(docid, db)
      assert doc_retrieved == doc_with_id
    end

  end

  describe "get!" do

    test "raise exception when id is not found in the database", context do
      db = context[:db]
      assert_raise BarrexError, fn ->
        Barrex.Database.get!("doesnotexist", db)
      end
    end

    test "returns doc when id found in the databas", context do
      db = context[:db]
      doc = %{"name" => "john", "v" => 1}
      {:ok, docid, _} =  Barrex.Database.post(doc, db)
      doc_with_id = Map.put(doc, "id", docid)
      doc_retrieved = Barrex.Database.get!(docid, db)
      assert doc_retrieved == doc_with_id
    end

  end

  test "basic operations", context do
    db = context[:db]
    doc = %{"name" => "john", "v" => 1}
    {:ok, docid, _} =  Barrex.Database.post(doc, db)
    {:ok, doc2, _} = Barrex.Database.get(docid, db)
    doc3 = %{doc2 | "v" => 2}
    Barrex.Database.put(doc3, db)
    Barrex.Database.delete(doc3, db)
  end

  test "retrieve all docs", context do
    db = context[:db]
    doc1 = %{"id" => "doc1", "v" => 1}
    doc2 = %{"id" => "doc2", "v" => 2}
    {:ok, _, _} =  Barrex.Database.post(doc1, db)
    {:ok, _, _} =  Barrex.Database.post(doc2, db)
    {:ok, docs} = Barrex.Database.docs(db)
    assert [doc1, doc2] == docs
    Barrex.Database.delete(doc1, db)
    Barrex.Database.delete(doc2, db)
  end

  test "changes feed", context do
    db = context[:db]
    stream = Barrex.Database.changes(db)

    doc = %{"id" => "change", "name" => "john", "v" => 2}
    Barrex.Database.post(doc, db)

    res = stream
    |> Stream.take(1)
    |> Enum.to_list()

    assert [doc] = res
    Barrex.Database.delete(doc, db)
  end

  test "changes feed with meta", context do
    db = context[:db]
    stream = Barrex.Database.changes(db, [meta: true])

    doc = %{"id" => "change", "name" => "john", "v" => 2}
    Barrex.Database.post(doc, db)

    res = stream
    |> Stream.take(1)
    |> Enum.to_list()

    [doc_meta] = res
    assert Map.keys(doc_meta) == ["doc", "meta"]
    assert Map.keys(doc_meta["meta"]) == ["changes", "id", "rev", "rid", "seq"]
    assert doc_meta["doc"] == doc
    Barrex.Database.delete(doc, db)
  end

  test "changes since", context do
    db = context[:db]
    doc1 = %{"id" => "doc1", "v" => 1}
    doc2 = %{"id" => "doc2", "v" => 2}
    Barrex.Database.post(doc1, db)
    Barrex.Database.post(doc2, db)

    stream0 = Barrex.Database.changes(db, [meta: true])
    res0 = stream0
    |> Stream.take(2)
    |> Enum.to_list()

    [d1, d2] = res0
    assert d1["meta"]["seq"] == 1
    assert d2["meta"]["seq"] == 2

    stream1 = Barrex.Database.changes(db, [meta: true, since: 1])
    res1 = stream1
    |> Stream.take(1)
    |> Enum.to_list()

    [d2] = res1
    assert d2["meta"]["seq"] == 2

    Barrex.Database.delete(doc1, db)
    Barrex.Database.delete(doc2, db)
  end

  defp db_url do
    barrel_url() <> "/dbs/barrex"
  end

  defp barrel_url do
    case System.get_env("BARREL_URL") do
      :nil -> "http://127.0.0.1:7080"
      url -> url
    end
  end

end
