defmodule Barrex do

  def create_database(url) do
    :barrel_httpc.create_database(url)
  end

  def delete_database(url) do
    :barrel_httpc.delete_database(url)
  end

end

defmodule Barrex.Database do
  require Logger

  def open(url) do
    :barrel_httpc.connect(url)
  end

  def get(docid, db) do
    :barrel_httpc.get(db, docid, [])
  end

  def get!(docid, db) do
    case :barrel_httpc.get(db, docid, []) do
      {:ok, doc, _} -> doc
      _ -> raise BarrexError, message: "docid not found " <> docid
    end
  end

  def post(doc, db) do
    :barrel_httpc.post(db, doc, [])
  end

  def put(doc, db) do
    :barrel_httpc.put(db, doc, [])
  end

  def delete(doc, db) do
    docid = doc["id"]
    :barrel_httpc.delete(db, docid, [])
  end

  def docs(db) do
    fun = fn(doc, _meta, acc1) ->
      {:ok, [doc | acc1]}
    end
    {:ok, docs} = :barrel_httpc.fold_by_id(db, fun, [], [])
    {:ok, Enum.reverse(docs)}
  end

  def changes(db, options \\ []) do
    Stream.resource(fn -> connect_changes(db, options) end,
                    fn(acc) -> recv_changes(acc, options) end,
                    fn(acc) -> end_changes(acc) end)
  end

  defp connect_changes(db, options) do
    parent = self()
    callback = fn(change) ->
      send(parent, {:barrel_change, self(), change})
    end
    since = Keyword.get(options, :since) || 0
    options = %{since: since, mode: :sse, changes_cb: callback}
    Process.flag(:trap_exit, true)
    {:ok, pid} = :barrel_httpc_changes.start_link(db, options)
    {pid, db}
  end

  defp recv_changes({_pid, db}=acc, options) do
    receive do
      {:barrel_change, _pid, %{<<"deleted">> => true}} ->
        {[], acc}
      {:barrel_change, _pid, change} ->
        {:ok, doc, _} = change |> Map.get("id") |> Barrex.Database.get(db)
        case Keyword.get(options, :meta) do
          true ->
            {[%{"doc" => doc, "meta" => change}], acc}
          _ ->
            {[doc], acc}
        end
      {:ack, _, {:ok, _}} ->
        recv_changes(acc, options)
      error ->
        Logger.error "#{__MODULE__} received #{inspect error}"
        {:halt, acc}
    end
  end

  defp end_changes({pid, _db}) do
    Process.flag(:trap_exit, false)
    :ok = :barrel_httpc_changes.stop(pid)
    :ok
  end

end

defmodule BarrexError do
  defexception [:message]

  def exception(value) do
    msg = "barrex error: #{inspect value}"
    %BarrexError{message: msg}
  end
end
